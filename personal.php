<?php
include 'header.php';
if(isset($_GET['user'])){
	$id = $_GET['user'];

}else{
	isset($_SESSION['user']['id']) ? $id = $_SESSION['user']['id'] : header('location: index.php');
}
$sql = "SELECT id, mail, name, nickname, introduce, sex, image FROM users where id = ?";
$stmt = $db->prepare($sql);
$stmt->execute(array($id));
$user = $stmt->fetch();
if(!$user){
	header('location: index.php');
}
$sql = "SELECT * FROM articles WHERE user_id = ?";
$stmt = $db->prepare($sql);
$stmt->execute(array($id));
$data = $stmt->fetchAll();

$introduce = $user['introduce'];
$user['nickname'] == "" ? $nickname = "" : $nickname = "( ".$user['nickname']." )";
?>
<section id="body">
	<div class="container mb-5">
		<div class="row mt-5">
			<nav>
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
			    <li class="breadcrumb-item">個人主頁</li>
			    <li class="breadcrumb-item active"><?=pr($user['name'])?></li>
			  </ol>
			</nav>
		</div>
		<div class="row" id="userDetailBox">
			<div class="col-md-12 mb-2">
				<div class="row">
					<?php
						$user['introduce'] == "" ? $introduce = "<p class='text-secondary'>Oops! 看來作者還沒更新自我介紹 :( </p>" : $introduce = pr($user['introduce']);

						if($user['image'] != ""){

					?>
							<div id="userImageForm" class="col-4 pt-2">
								<img id="userImage" class="col-lg-12 mb-2 imgBox" src="<?=USER_IMG_DIR.pr($user['image'])?>">
							</div>
							<div class="col-8" id="userDetailForm">
								<div class="form-group">																	
									<h4 class="pt-2 mb-0"><?=pr($user['name'])?> <small class="text-secondary"><?=pr($nickname)?></small></h4>
									<hr>
									<div><?=$introduce?></div>
								</div>
							</div>
							<?php
							}else{
							?>
							<div class="col-12" id="userDetailForm">
								<div class="form-group">

									<div>
										<h4 class="pt-2 mb-0"><?=pr($user['name'])?> <small class="text-secondary"><?=pr($nickname)?></small></h4>
										<br>
										<?=$introduce?>		
									</div>
								</div>
							</div>
					<?php } ?>
				</div>
			</div>
		</div>

		
		<div class="row">
			<?php
			if(count($data) == 0){
			?>
				
				<div class="emptyCategory col-lg-12 text-center">Oops! 他尚未發表任何遊記!</div>
				
			<?php
			}else{
			?>
				
				<div class="col-lg-12 text-center mb-2 mt-2"><h4>矚目焦點</h4></div>
				<div class="col-lg-12 text-center mb-5">看看他最近都去哪 ?</div>
				
			<?php
			}
			foreach($data as $d){
				$cover = $d['cover'];
			?>
			<div class="col-md-4 col-lg-3 mb-2">
				<div class="card articleCard">
				  <img class="card-img-top" src="<?=COVER_DIR.pr($cover)?>" alt="Card image cap">
				  <div class="card-body">
				  	<h5 class="card-title"><a href="article.php?id=<?=pr($d['id'])?>"><?=pr($d['title'])?></a></h5>
				    <p class="card-text"><small><?=pr($d['create_at'])?></small></p>
				    <p class="card-text"><?=pr(mb_substr($d['content'],0 ,30, "UTF-8"))?>...</p>
				  </div>

				</div>
			</div>
			<?php
			}
			?>
			
		</div>

	</div>

</section>
<?php
include 'footer.php';
?>