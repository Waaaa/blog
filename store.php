<?php
include 'include/config.php';
if(!isset($_SESSION['user']['id'])){
	header('location: index.php');
	exit;
}
if($_SERVER['REQUEST_METHOD'] == "POST"){
	
	if($_GET['a'] == "createArticle"){

		$fields = ['title', 'region', 'content'];
		$image_fields = ['cover', 'images'];
		$error = 0;
		foreach ($fields as $field) {
			if(!isset($_POST[$field]) || $_POST[$field] == ""){
				$error += 1;
				echo $field;
			}
		}

		if($_FILES['cover']['name'] == ""){
			$error += 1;
		}

		if($error > 0){
			$message = ['error' => '所有欄位皆為必填'];
			$_SESSION['error'] = $message;
			$_SESSION['form'] = $_POST;
			header("location: editor.php");
			exit;
		}

		$stmt = $db->prepare("INSERT INTO articles VALUES('', ?, ?, ?, ?, ?, ?, ?)");
		$d = new DateTime();
		$time = $d->format('y-m-d');
		$title = $_POST['title'];
		$content = $_POST['content'];
		$region = $_POST['region'];
		$filename = rand(1,100).$_FILES['cover']['name'];
		move_uploaded_file($_FILES['cover']['tmp_name'], COVER_DIR.$filename);
		$stmt->execute(array($title, $content, $region, $filename, $time, $time, $_SESSION['user']['id']));
		$id = $db->lastInsertId();
		if(isset($_FILES['images'])){
			if (!file_exists(IMG_DIR.$id)) {
			    mkdir(IMG_DIR.$id, 0777, true);
			}
			$path = IMG_DIR.$id.'/';
			for($i = 0 ; $i < count($_FILES['images']['name']) ; $i++){
				if($_FILES['images']['name'][$i] != ""){
					try{
						$stmt = $db->prepare("INSERT INTO images VALUES('', ?, ?)");
						$filename = rand(1,100).$_FILES['images']['name'][$i];
						$stmt->execute(array($filename, $id));
						move_uploaded_file($_FILES['images']['tmp_name'][$i], $path.$filename);
					}catch(Exception $e){
						echo "";
					}
				}
			}
		}

		
		$message = ['success' => '新增文章成功'];
		$_SESSION['success'] = $message;
		header("location: personal.php");
		exit;
	}elseif ($_GET['a'] == "editArticle") {		
		if(! isset($_GET['id'] )){
			header("location: index.php");
			exit;
		}
		$error = 0;
		$fields = ['title', 'region', 'content'];
		foreach ($fields as $field) {
			if(!isset($_POST[$field]) || $_POST[$field] == ""){
				$error += 1;
			}
		}
		$article_id = $_GET['id'];
		if($error > 0){
			$message = ['error' => '所有欄位皆為必填'];
			$_SESSION['error'] = $message;
			$_SESSION['form'] = $_POST;
			header("location: editor.php?a=editArticle&id=".$article_id);
			exit;
		}
		
		$user_id = $_SESSION['user']['id'];
		$stmt = $db->prepare("UPDATE articles SET title = ?, content = ?, region = ?, update_at = ? WHERE id = '$article_id' and user_id = ?");
		$d = new DateTime();
		$time = $d->format('y-m-d');
		$title = $_POST['title'];
		$content = $_POST['content'];
		$region = $_POST['region'];
		$stmt->execute(array($title, $content, $region, $time, $user_id));
		if($_POST['changeCover'] == "y"){
			$sql = "SELECT cover FROM articles WHERE id = ? and user_id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($article_id, $user_id));
			$old_filename = $stmt->fetch();

			@unlink(COVER_DIR.$old_filename['cover']);
			$filename = rand(1,100).$_FILES['cover']['name'];
			move_uploaded_file($_FILES['cover']['tmp_name'], COVER_DIR.$filename);
			$stmt = $db->prepare("UPDATE articles SET cover = ? WHERE id = ? and user_id = ?");
			$stmt->execute(array($filename, $article_id, $user_id));
			
		}
		if(isset($_POST['deleteImg'])){
			//echo 'get';
			foreach ($_POST['deleteImg'] as $image_id) {
				$sql = "SELECT filename FROM images WHERE id = ?";
				$stmt = $db->prepare($sql);
				$stmt->execute(array($image_id));
				$filename = $stmt->fetch();
				@unlink(IMG_DIR.$article_id .'/'.$filename['filename']);
				$sql = "DELETE FROM images WHERE id = ?";
				$stmt = $db->prepare($sql);
				$stmt->execute(array($image_id));
				
			}

		}
		if(isset($_FILES['images'])){
			//echo "string";
			if (!file_exists(IMG_DIR.$article_id)) {
			    mkdir(IMG_DIR.$article_id, 0777, true);
			}
			$path = IMG_DIR.$article_id.'/';
			for($i = 0 ; $i < count($_FILES['images']['name']) ; $i++){
				if($_FILES['images']['name'][$i] != ""){
					try{
						$stmt = $db->prepare("INSERT INTO images VALUES('', ?, ?)");
						$filename = rand(1,100).$_FILES['images']['name'][$i];
						$stmt->execute(array($filename, $article_id));
						move_uploaded_file($_FILES['images']['tmp_name'][$i], $path.$filename);
					}catch(Exception $e){
						echo "";
					}
				}
			}
		}

		
		
		$message = ['success' => '更新文章成功'];
		header("location: personal.php");
		exit;
	
	}elseif ($_GET['a'] == "editPerdata") {
		if($_GET['b'] == "userImage"){
			if(!empty($_FILES['userImageInput']['name'])){
				$error = 0;
				
				$filename = $_FILES['userImageInput']['name'];
				try{
					move_uploaded_file($_FILES['userImageInput']['tmp_name'], USER_IMG_DIR.$filename);
				}catch(Exception $e){
					$message = ['userImageInput' => '發生錯誤，請再上傳一次'];
					$_SESSION['error'] = $message;
					header('location: acc.php');
					exit;
				}
				$stmt = $db->prepare("UPDATE users SET image = ? WHERE id = ? ");
				$stmt->execute(array($filename, $_SESSION['user']['id']));
				$_SESSION['user']['image'] = $filename;
				$message = ['userImageInput' => '上傳成功'];
				$_SESSION['success'] = $message;
				header('location: acc.php');
				exit;
			}else{
				$message = ['userImageInput' => '請選擇圖片'];
				$_SESSION['error'] = $message;
				header('location: acc.php');
				exit;
			}
		}elseif ($_GET['b'] == "userData") {
			$error = 0;
			$username = $_POST['username'];
			$sql = "SELECT id FROM users WHERE name = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($username));
			$username_reapt = $stmt->fetch();
			if(strlen($username) < 3){
				$error += 1;
				$message['username'] = "使用者名稱最少要三個字";
			}elseif($username_reapt && $username != $_SESSION['user']['name']){
				$error += 1;
				$message['username'] = "使用者名稱已存在";
			}
			$nickname = $_POST['nickname'];
			$sql = "SELECT id FROM users WHERE name = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($username));
			$nickname_reapt = $stmt->fetch();
			if(strlen($nickname) < 3){
				$error += 1;
				$message['nickname'] = "使用者暱稱最少要三個字";
			}elseif($nickname_reapt && $nickname != $_SESSION['user']['nickname']){
				$error += 1;
				$message['nickname'] = "使用者暱稱已存在";
			}
			$content = $_POST['introduce'];
			if($error > 0){
				$_SESSION['error'] = $message;
				header('location: acc.php');
				exit;
			}else{
				$stmt = $db->prepare('UPDATE users SET name = ?, nickname = ?, introduce = ? WHERE id = ?');
				$stmt->execute(array($username, $nickname, $content, $_SESSION['user']['id']));
				$id = $_SESSION['user']['id'];
				$sql = "SELECT id, name, nickname, mail, introduce, sex, image FROM users WHERE id = ?";
				$stmt = $db->prepare($sql);
				$stmt->execute(array($id));
				$data = $stmt->fetch();
				$_SESSION['user'] = $data;
				$message = ['userData' => '資料更新成功'];
				$_SESSION['success'] = $message;
				header('location: acc.php');
				exit;
			}
			//print_r($_POST);
			

		}elseif ($_GET['b'] == "userPassword") {
			$id = $_SESSION['user']['id'];
			$sql = "SELECT id, password FROM users WHERE id = '$id'";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($id));
			$user = $stmt->fetch();
			if(!isset($user['id'])){
				header('location: index.php');
				exit;
			}
			if($_POST['old_password'] == "" || $_POST['new_password']  == ""){
				$error += 1;
				$_SESSION['error'] = ['password' => '請輸入密碼'];
				header('location: acc.php');
				exit;
			}
			if(password_verify($_POST['old_password'] , $user['password'])){
				$stmt = $db->prepare('UPDATE users SET password = ? WHERE id = ?');
				$passwdHash = password_hash($_POST['new_password'], PASSWORD_DEFAULT);
				$stmt->execute(array($passwdHash, $_SESSION['user']['id']));
				$message = ['password' => '密碼更新成功'];
				$_SESSION['success'] = $message;
				header('location: acc.php');
				exit;
			}
			$_SESSION['error'] = ['password' => '密碼錯誤!'];
			header('location: acc.php');
			exit;
			
		}
	}
	
}else{
	if ($_GET['a'] == "deleteArticle") {
		if(! isset($_GET['id'] )){
			header("location: index.php");
			exit;
		}
		$article_id = $_GET['id'];
		$user_id = $_SESSION['user']['id'];
		if($user_id != 0){
			$sql = "SELECT id, cover FROM articles WHERE id = ? and user_id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($article_id, $user_id));
		}else{
			$sql = "SELECT id, cover FROM articles WHERE id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($article_id));
		}	
		$data = $stmt->fetch();
		if(isset($data['id'])){
			unlink(COVER_DIR.$data['cover']);

			$sql = "SELECT filename FROM images WHERE article_id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($article_id));
			$images = $stmt->fetchAll();
			foreach ($images as $image) {
				unlink(IMG_DIR.$article_id .'/'.$image['filename']);
			}
			rmdir(IMG_DIR.$article_id);
			$sql = "DELETE FROM images WHERE article_id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($article_id));
			if($user_id != 0){
				$sql = "DELETE FROM articles WHERE id = ? and user_id = ?";
				$stmt = $db->prepare($sql);
				$stmt->execute(array($article_id, $user_id));
			}else{
				$sql = "DELETE FROM articles WHERE id = ?";
				$stmt = $db->prepare($sql);
				$stmt->execute(array($article_id));
			}
			$message = ['success' => '刪除文章成功'];
		}
		header("location: list.php");
		exit;
		return;
	}elseif($_GET['a'] == "editUser"){
		if(! isset($_GET['id'] )){
			header("location: index.php");
			exit;
		}
		if($_SESSION['user']['id'] != 0 ){
			header("location: index.php");
			exit;
		}
		$id = $_GET['id'];
		$sql = "SELECT authority FROM users WHERE id = ?";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($id));
		$user = $stmt->fetch();
		if($user['authority']){
			$sql = "UPDATE users SET authority = 0 WHERE id = ?";
		}else{
			$sql = "UPDATE users SET authority = 1 WHERE id = ?";
		}
		$stmt = $db->prepare($sql);
		$stmt->execute(array($id));
		header("location: userlist.php");
		exit;
	}elseif($_GET['a'] == "deleteUser"){
		if(! isset($_GET['id'] )){
			header("location: index.php");
			exit;
		}
		if($_SESSION['user']['id'] != 0 ){
			header("location: index.php");
			exit;
		}
		$id = $_GET['id'];
		$sql = "SELECT * FROM articles WHERE user_id = ?";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($id));
		$articles = $stmt->fetchAll();
		foreach ($articles as $data) {
			$article_id = $data['id'];
			unlink(COVER_DIR.$data['cover']);
			$sql = "SELECT filename FROM images WHERE article_id = '$article_id'";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($id));
			$images = $stmt->fetchAll();
			foreach ($images as $image) {
				unlink(IMG_DIR.$article_id .'/'.$image['filename']);
			}
			rmdir(IMG_DIR.$article_id);
			$sql = "DELETE FROM images WHERE article_id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($article_id));

			$sql = "DELETE FROM articles WHERE id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($article_id));
		}
		$sql = "DELETE FROM users WHERE id = ?";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($id));
		header("location: userlist.php");
		exit;
	}else{
		header("location: index.php");	
		exit;
	}
	
}