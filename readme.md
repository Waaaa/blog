# Walk walk!–旅遊部落格管理系統

## 系統說明
### (一)、前端功能模組
**申請會員**  
使用者輸入使用者暱稱、電子郵件、密碼及選擇性別並註冊成為網站會員。

**登入會員**  
使用者輸入電子郵件、密碼來登入網站。

**網站首頁**  
首頁將會列出目前所有的遊記，列出遊記之封面相片、標題、新增時間與遊記內文擷取，並以新增時間遞減排序。

**特定地區遊記查詢**  
系統預設以台灣的地理位置來分類遊記，選項有：北部、中部、南部、東部及離島地區，使用者可以在導覽列選擇特定地區，以檢視分類過的遊記，遊記呈現方式同首頁。

**遊記瀏覽**  
點選遊記的標題即可檢視特定遊記的完整內容，系統將會列出以下資訊：
* 相簿(多張相片輪播，可手動選擇相片檢視)
* 標題
* 最後更新時間
* 內文
* 作者資訊(大頭照、作者名稱、暱稱及自我介紹)

**新增遊記**  
註冊為系統會員的使用者可以新增遊記，系統要求以下資訊：
* 標題
* 地區分類(北部、中部、南部、東部及離島地區)
* 封面相片(提供預覽功能)
* 內文
* 上傳相簿圖片(0~多張，提供預覽功能)

**管理遊記**  
系統會員可以在管理遊記頁面管理自己新增過的遊記，系統將列出以下資訊：
* 標題
* 新增時間
* 最後更新時間
並提供修改及刪除遊記的功能

**修改遊記**
系統會員可以修改自己新增過的文章，系統可以讓會員修改以下資訊：
* 標題
* 地區分類(北部、中部、南部、東部及離島地區)
* 封面相片(提供預覽功能)
* 內文
* 上傳相簿圖片(0~多張，提供預覽功能)

**個人頁面**  
點擊作者資訊中的作者名稱即可連結至作者的個人頁面，系統將列出以下資訊：
* 大頭照
* 作者名稱
* 暱稱
* 自我介紹
* 建立的遊記

**修改個人頁面**  
系統會員可以管理自己的個人資料，系統可以讓會員修改以下資訊：
* 大頭照
* 作者名稱
* 暱稱
* 自我介紹

### (二)、後端管理模組

**使用者管理**  
管理者可以停權及刪除系統的會員。
* 停權：無法登入系統直到管理者復權該會員
* 刪除：刪除會員的資料及新增過的遊記(無法復原)

**刪除遊記**  
管理者可以刪除系統的所有遊記。


## 開發工具與設備環境
* bootstrap 4.0.0
* jquery 3.3.1
* PHP 7.3.5
* Apache 2.4
* MariaDB 10.1.40

## 資料庫規劃
```sql
CREATE DATABASE IF NOT EXISTS `blog`;

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `content` text,
  `region` varchar(10) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `create_at` date DEFAULT CURRENT_TIMESTAMP,
  `update_at` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT userFK FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='遊記資料表';

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT aricleFK FOREIGN KEY(`article_id`) REFERENCES `article`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='相片資料表';

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(254) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(10) NOT NULL,
  `nickname` varchar(10),
  `introduce` text,
  `sex` char(1) NOT NULL,
  `image` varchar(255) NOT NULL,
  `authority` tinyint(1) DEFAULT '0',
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='使用者資料表';
```
