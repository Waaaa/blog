<?php
include 'include/config.php';
?>
<head>
	<title>Walk walk!</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link rel="stylesheet" href="asset/css/css.css"></link>
</head>
<body>
<section id="header">
	<nav class="navbar navbar-expand-lg navbar-light">
		<a class="navbar-brand" href="#"><h1>Walk walk!</h1></a>
	</nav>
	<!--
	<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
	-->
	<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
		<div class="container">
			<a class="navbar-brand" href="index.php"><h1>Walk walk!</h1></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    	<span class="navbar-toggler-icon"></span>
		    </button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item <?= isset($_GET['region']) ? "" : "active"; ?>">
						<a class="nav-link" href="index.php">首頁</a>
					</li>
					<li class="nav-item <?= @$_GET['region'] == "north" ? "active" : ""; ?>">
						<a class="nav-link" href="index.php?region=north">北部</a>
					</li>
					<li class="nav-item <?= @$_GET['region'] == "center" ? "active" : ""; ?>">
						<a class="nav-link" href="index.php?region=center">中部</a>
					</li>
					<li class="nav-item <?= @$_GET['region'] == "south" ? "active" : ""; ?>">
						<a class="nav-link" href="index.php?region=south">南部</a>
					</li>
					<li class="nav-item <?= @$_GET['region'] == "east" ? "active" : ""; ?>">
						<a class="nav-link" href="index.php?region=east">東部</a>
					</li>
					<li class="nav-item <?= @$_GET['region'] == "island" ? "active" : ""; ?>">
						<a class="nav-link" href="index.php?region=island">離島地區</a>
					</li>
				</ul>
				<ul class="navbar-nav my-2 my-lg-0">					
					<?php

					if(isset($_SESSION['user'])){
						if($_SESSION['user']['id'] != 0){
					
							echo '
							<li id="dropdown" class="nav-item dropdown">
						        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						          Hello! '.pr($_SESSION['user']['name']).' <i class="fas fa-user"></i>
						        </a>
						        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
						          <a class="nav-link nav-link-item pl-2" href="editor.php">新增文章</a>
						          <a class="nav-link nav-link-item pl-2" href="list.php">管理文章</a>
						          <div class="dropdown-divider"></div>
						          <a class="nav-link nav-link-item pl-2" href="personal.php?user='.pr($_SESSION['user']['id']).'">個人主頁</a>
						          <div class="dropdown-divider"></div>
						          <a class="nav-link nav-link-item pl-2" href="acc.php">帳號設定</a>
						        </div>
						    </li>
							<li class="nav-item">
								<a class="nav-link" href="auth.php?a=logout">登出</a>
							</li>';
						}else{
							echo '
							<li id="dropdown" class="nav-item dropdown">
						        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						          Hello! '.pr($_SESSION['user']['name']).' <i class="fas fa-user"></i>
						        </a>
						        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
						          <a class="nav-link nav-link-item pl-2" href="userlist.php">使用者管理</a>
						          <a class="nav-link nav-link-item pl-2" href="list.php">文章管理</a>
						        </div>
						    </li>
							<li class="nav-item">
								<a class="nav-link" href="auth.php?a=logout">登出</a>
							</li>';
						}	
					}else{
						echo '<li class="nav-item">
									<a class="nav-link" href="login.php">登入</a>
							  </li>
							  <li class="nav-item">
									<a class="nav-link" href="register.php">註冊</a>
							  </li>';
					}
					?>
				</ul>

			</div>
		</div>
	</nav>
	
</section>	