<?php
include 'header.php';
if(!isset($_SESSION['user']['id'])){
	header('location: index.php');
}
$id = $_SESSION['user']['id'];

//管理者
if($id == 0){
	$data = $db->query("SELECT * FROM articles")->fetchAll();
	$currname = '';
}else{
	$sql = "SELECT * FROM articles WHERE user_id = ?";
	$stmt = $db->prepare($sql);
	$stmt->execute(array($id));
	$data = $stmt->fetchAll();
	$currname = $_SESSION['user']['name']."的";
}




?>
<section id="body">
	<div class="container mb-5">
		<div class="row mt-5">
			<nav>
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
			    <li class="breadcrumb-item">管理文章</li>
			    <li class="breadcrumb-item active"><?=pr($currname)?>文章列表</li>
			  </ol>
			</nav>
		</div>
		<div class="row" id="listBox">
			<?php
			if(count($data) == 0){
				echo '<div class="emptyCategory col-lg-12 text-center">你還沒有新增任何的文章喔!</div>';
			}
			?>
			<table class="table">
				<?php
				for($i = 0 ; $i < count($data); $i++){
					echo "<tr >";
					$noborder = '';
					if($i == 0){
						$noborder = 'style="border-top: 0;"';
					}
					$index = $i + 1;
					$onclick = 'return confirm("確定要刪除文章嗎? 系統將無法復原您的刪除")';
					echo '<th '.$noborder.' scope="row">'.pr($index).'</th>';
					echo "<td ".$noborder.">".pr($data[$i]['title'])."</td>";
					echo "<td ".$noborder.">新增於 ".pr($data[$i]['create_at'])."</td>";
					echo "<td ".$noborder.">最後更新於 ".pr($data[$i]['update_at'])."</td>";
					if($id == 0){
						$user_id = $data[$i]['user_id'];
						//抓出文章作者
						$sql = "SELECT name FROM users WHERE id = ?";
						$stmt = $db->prepare($sql);
						$stmt->execute(array($user_id));
						$author = $stmt->fetch();
						echo "<td ".$noborder.">".pr($author['name'])."</td>";
					}else{
						echo "<td ".$noborder."><a href='editor.php?a=editArticle&id=".pr($data[$i]['id'])."'>修改</a></td>";
					}			
					echo "<td ".$noborder."><a href='store.php?a=deleteArticle&id=".pr($data[$i]['id'])."' onclick='".$onclick."'>刪除</a></td>";
					echo "</tr >";
				}
				?>
			</table>
		</div>

	

	</div>

</section>
<?php
include 'footer.php';
?>