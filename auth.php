<?php
include 'include/config.php';

if($_SERVER['REQUEST_METHOD'] == "POST"){
	if($_GET['a'] == "register"){
		//csrf
		if(!isset($_SESSION['csrf']['register']) || $_SESSION['csrf']['register'] !== $_POST['csrf']){
			header('HTTP/1.1 403 Forbidde');
			exit;
		}
		unset($_SESSION['csrf']['register']);
		if(isset($_SESSION['error'])){
			unset($_SESSION['error']);
		}
		$error = 0;
		$message = array();
		$username = $_POST['username'];

		//判斷有沒有重複的使用者
		$sql = "SELECT id FROM users WHERE name = ?";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($username));
		$username_reapt = $stmt->fetch();
		if(strlen($username) < 3){
			$error += 1;
			$message['username'] = "使用者名稱最少要三個字";
		}elseif($username_reapt){
			$error += 1;
			$message['username'] = "使用者名稱已存在";
		}

		//判斷有沒有重複的電子郵件
		$emailCheck = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
		$sql = "SELECT id FROM users WHERE mail = ?";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($emailCheck));
		$email_reapt = $stmt->fetch();
		if(! $emailCheck){
			$error += 1;
			$message['email'] = "email格式錯誤";
		}elseif($email_reapt){
			$error += 1;
			$message['email'] = "Email已註冊";
		}
		if(strlen($_POST['passwd']) < 5){
			$error += 1;
			$message['passwd'] = "密碼最少要五個字";
		}
		if($_POST['sex'] != "女" && $_POST['sex'] != "男"){
			$error += 1;
			$message['sex'] = "請選擇正確的性別";
		}
		if($error > 0){
			$_SESSION['error'] = $message;
			header('location: register.php');
			exit;
		}else{
			$passwdHash = password_hash($_POST['passwd'], PASSWORD_DEFAULT);
			if($passwdHash !== false){
				$stmt = $db->prepare("INSERT INTO users VALUES('', ?, ?, ? ,'', '', ?, '', '0')");
				$stmt->execute(array( $_POST['email'], $passwdHash, $username, $_POST['sex']));
				$id = $db->lastInsertId();
				$data = $db->query("SELECT id, name, nickname, mail, introduce, sex, image FROM users WHERE id = '$id'")->fetch();
				$_SESSION['user'] = $data;
				header('location: index.php');
			}
			
		}

	}elseif($_GET['a'] == "login"){
		//csrf
		if(!isset($_SESSION['csrf']['login']) || $_SESSION['csrf']['login'] !== $_POST['csrf']){
			header('HTTP/1.1 403 Forbidde');
			exit;
		}
		unset($_SESSION['csrf']['login']);
		$error = "";
		$emailCheck = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
		$sql = "SELECT id FROM users WHERE mail = ?";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($emailCheck));
		$email_reapt = $stmt->fetch();
		if(! $emailCheck || $email_reapt == ""){
			$error += 1;
		}
		if(strlen($_POST['passwd']) == 0){
			$error += 1;
		}
		if($error > 0){
			$message['login'] = "使用者名稱或密碼錯誤";
			$_SESSION['error'] = $message;
			header('location: login.php');
			exit;
		}else{
			$mail = $_POST['email'];
			$sql = "SELECT id, password, authority FROM users WHERE mail = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($mail));
			$user = $stmt->fetch();

			if($user && password_verify($_POST['passwd'] , $user['password']) && !$user['authority']){
				$sql = "SELECT id, name, nickname, mail, introduce, sex, image FROM users WHERE id = ?";
				$stmt = $db->prepare($sql);
				$stmt->execute(array($user['id']));
				$data = $stmt->fetch();
				//print_r($data);
				$_SESSION['user'] = $data;
				header('location: index.php');
				exit;
			}else{
				$message['login'] = "使用者名稱或密碼錯誤";
				$_SESSION['error'] = $message;
				header('location: login.php');
				exit;
			}
		}
	}elseif($_GET['a'] == "admin"){
		$error = "";

		if(strlen($_POST['passwd']) == 0 || strlen($_POST['acc']) == 0){
			$error += 1;
		}
		if($error > 0){
			$message['login'] = "使用者名稱或密碼錯誤";
			$_SESSION['error'] = $message;
			header('location: admin.php');
			exit;
		}else{
			
			if($_POST['acc'] == "administrator" && $_POST['passwd'] == "12345"){
				//print_r($data);
				$_SESSION['user']['id'] = 0;
				$_SESSION['user']['name'] = "administrator";
				header('location: index.php');
				exit;
			}else{
				$message['login'] = "使用者名稱或密碼錯誤";
				$_SESSION['error'] = $message;
				header('location: admin.php');
				exit;
			}
		}
	}else{
		header('location: index.php');
		exit;
	}
}else{
	if($_GET['a'] == "logout"){	
		if(isset($_SESSION['user'])){
			unset($_SESSION['user']);
		}
	}
	header('location: index.php');
	exit;
}