4<?php
include 'header.php';


if(!isset($_SESSION['user']['id'])){
	header('location: index.php');
}
$id = $_SESSION['user']['id'];
$sql = "SELECT id, mail, name, nickname, introduce, sex, image FROM users where id = ?";
$stmt = $db->prepare($sql);
$stmt->execute(array($id));
$user = $stmt->fetch();
$user['nickname'] == "" ? $nickname = "" : $nickname = "( ".$user['nickname']." )";
?>
<section id="body">
	<div class="container mb-5">
		<div class="row mt-5">
			<nav>
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
			    <li class="breadcrumb-item">帳號設定</li>
			    <li class="breadcrumb-item active"><?=pr($user['name'])?></li>
			  </ol>
			</nav>
		</div>
		<div class="row" id="userDetailBox">
			<div class="col-md-12 mb-2">
				<div class="row">
					<div class="col-12 mt-4 mb-2">
						<h5 class="font-weight-bold">基本設定</h5>
					</div>
					<form id="userImageForm" method="POST" enctype="multipart/form-data" action="store.php?a=editPerdata&b=userImage" class="col-4">
						<?php
						if(!empty($user['image'])){
							echo '<img id="userImage" class="mt-4 mb-2 imgBox col-12" src="'.USER_IMG_DIR.pr($user['image']).'">';
						}else{
							echo '<img id="userImage" class="mb-2 col-12">';
						}
						
						?>	

						<div class="form-group">
							<label>大頭貼</label>
							<input id="userImageInput" name="userImageInput" class="form-control" type="file">
						</div>
						
						<div class="form-group text-right">
							<?php
							if(isset($_SESSION['error']['userImageInput'])){
								echo '<lable class="text-danger">'.pr($_SESSION['error']['userImageInput']).'</lable>';
								unset($_SESSION['error']);
							}elseif(isset($_SESSION['success']['userImageInput'])){
								echo '<lable class="text-success">'.pr($_SESSION['success']['userImageInput']).'</lable>';
							}
							?>
							<button type="submit" class="btn btn-primary">上傳</button>
						</div>
						
					</form>
					<form class="col-8" id="userDetailForm" method="POST" action="store.php?a=editPerdata&b=userData">
						<div class="form-group">
							<label>Username</label>
							<input name="username" class="form-control" type="text" value="<?=@$user['name']?>" readonly>
							<?php
							if(isset($_SESSION['error']['username'])){
								echo "<label class='mt-1 text-danger'>".pr($_SESSION['error']['username'])."</label>";
							}
							?>
						</div>
						<div class="form-group">
							<label>Nickname</label>
							<input name="nickname" class="form-control" type="text" value="<?=@$user['nickname']?>" readonly>
							<?php
							if(isset($_SESSION['error']['nickname'])){
								echo "<label class='mt-1 text-danger'>".pr($_SESSION['error']['nickname'])."</label>";
								
							}
							?>
						</div>
						<hr>
						<div class="form-group">
							<label>關於我</label>
							<textarea name="introduce" class="form-control" rows="6" readonly><?=@$user['introduce']?></textarea>
						</div>
						<div id="button-group" class="form-group text-right">
							<?php
							if(isset($_SESSION['success']['userData'])){
								echo '<lable class="text-success">'.pr($_SESSION['success']['userData']).'</lable>';
							}					
							?>
							<button type="button" id="editDateBtn" class="btn">修改個人資料</button>
						</div>
					</form>
					
					<div class="col-12 mb-2">
						<hr>
						<h5 class="mt-4 font-weight-bold">重設密碼</h5>
						<form  method="POST" action="store.php?a=editPerdata&b=userPassword">
							<div class="form-group">
								<div class="col-12 col-md-4 mt-4 mb-2  pl-0">
									<label>請輸入舊密碼</label>
									<input type="password" name="old_password" class="form-control">
								</div>
								<div class="col-12 col-md-4 pl-0">
									<label>請輸入新密碼</label>
									<input type="password" name="new_password" class="form-control">
									<?php
									if(isset($_SESSION['error']['password'])){
										echo "<label class='mt-1 text-danger'>".pr($_SESSION['error']['password'])."</label>";
										
									}
									?>
								</div>
								<div class="col-12 col-md-6 mt-4 mb-2  pl-0">
									<button type="submit" class="btn-primary btn">修改密碼</button>
									<?php
									if(isset($_SESSION['success']['password'])){
										echo '<lable class="text-success">'.pr($_SESSION['success']['password']).'</lable>';
									}					
									?>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		


</section>
<?php
include 'footer.php';
?>