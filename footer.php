<?php
if(isset($_SESSION['error'])){
	unset($_SESSION['error']);
}
if(isset($_SESSION['success'])){
	unset($_SESSION['success']);
}
if(isset($_SESSION['message'])){
	unset($_SESSION['message']);
}
if(isset($_SESSION['form'])){
	unset($_SESSION['form']);
}
?>
<section id="footer" class="bg-light">
	<div class="bg-dark text-white">
		<div class="container">
			<div class="row pt-5 pb-5 mb-4 pl-3">
				<div class="col-12 col-lg-12">
					<div class="row">
						<div class="col-12 col-lg-4">
							<h5 class="font-weight-bold text-uppercase mt-3 pl-3 mb-4">小組成員</h5>
						    <ul class="list-unstyled">
						      <li>
						        <a class="nav-link">0452060 傅宥勳</a>
						      </li>
						      <li>
						        <a class="nav-link">0524007 吳禹璇</a>
						      </li>
						      <li>
						        <a class="nav-link">0524027 吳沛霖</a>
						      </li>
						      <li>
						        <a class="nav-link">0524033 陳韻如</a>
						      </li>
						      <li>
						        <a class="nav-link">0524053 李佳蓉</a>
						      </li>
						</div>
						<div class="col-12 col-lg-4">
							<h5 class="font-weight-bold text-uppercase mt-3 pl-3 mb-4">全台走透透</h5>
						    <ul class="list-unstyled">
						      <li>
						        <a class="nav-link text-white" href="index.php?region=north">北部</a>
						      </li>
						      <li>
						        <a class="nav-link text-white" href="index.php?region=center">中部</a>
						      </li>
						      <li>
						        <a class="nav-link text-white" href="index.php?region=south">南部</a>
						      </li>
						      <li>
						        <a class="nav-link text-white" href="index.php?region=east">東部</a>
						      </li>
						      <li>
						        <a class="nav-link text-white" href="index.php?region=island">離島地區</a>
						      </li>
						    </ul>
						</div>
						<div class="col-12 col-lg-4">
							<h5 class="font-weight-bold text-uppercase mt-3 pl-3 mb-4">加入我們</h5>
						    <ul class="list-unstyled">
						      <li>
						        <a class="nav-link text-white" href="login.php">登入</a>
						      </li>
						      <li>
						        <a class="nav-link text-white" href="register.php">註冊</a>
						      </li>
						     
						    </ul>
						</div>
						
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
	<div class="container">
		<div class="row pb-4">
			<div class="col-12 text-center">Copyright &copy; 2018, Design by Wuyu</div>
		</div>
	</div>
</section>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="asset/js/normal.js"></script>
<script type="text/javascript" src="asset/js/article.js"></script>
</html>