<?php
include 'header.php';
if(isset($_GET['region'])){
	$location = $_GET['region'];
	if(in_array($location, $locations)){
		
		$index = array_search($_GET['region'], $locations);
		$locationName = $locationNames[$index];
		$sql = "SELECT * FROM articles WHERE region = ? ORDER BY id DESC";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($index));
		$data = $stmt->fetchAll();
	}else{
		header('location: index.php');
	}
}else{
	$location = 'news';
	$locationName = '讀萬卷書，行萬里路';
	$data = $db->query('SELECT * FROM articles ORDER BY id DESC')->fetchAll();
}
?>

<section id="body">
	<div id="<?=@$location?>Jumbotron" class="jumbotron mb-5 d-flex justify-content-center align-items-center">
		<div class="container text-center">
			<h2 class="display-4"><b><?=pr(@$locationName)?></b></h2>
			<!--
			<p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
			</p>-->
		</div>	
	</div>
	<div id="<?=pr(@$location)?>Content" class="container mb-5">
		<?php
			if(count($data) == 0){
		?>
		<div class="row">
			<div class="emptyCategory col-lg-12 text-center">Oops! 好像還沒有人發表遊記, 快搶第一名!</div>
		</div>
		<?php
			}else{
		?>
		<div class="row mb-5">
			<div class="col-lg-12 text-center mb-2"><h4>矚目焦點</h4></div>
			<div class="col-lg-12 text-center mb-2">最近大家都去哪 ?</div>
		</div>
		<div class="row">
			

			<?php
			
				foreach($data as $d){
					$cover = pr($d['cover']);
			?>
			<div class="col-12 col-lg-3 mb-2">
				<div class="card articleCard">
				  <img class="card-img-top" src="<?=COVER_DIR.$cover?>" alt="Card image cap">
				  <div class="card-body">
				  	<h5 class="card-title"><a href="article.php?id=<?=pr($d['id'])?>"><?=pr($d['title'])?></a></h5>
				    <p class="card-text"><small><?=pr($d['create_at'])?></small></p>
				    <p class="card-text"><?=mb_substr(pr($d['content']),0 ,30, "UTF-8")?>...</p>
				  </div>
				</div>
			</div>
			<?php
				}
			?>
			
		</div>
		<?php
			}
		?>
	</div>
<?php
include 'footer.php';
?>