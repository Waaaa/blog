<?php
include 'header.php';
if(!isset($_SESSION['user']['id'])){
	header('location: index.php');
}
if(@$_GET['a'] == "editArticle" ){
	if(!isset($_GET['id'])){
		header('location: index.php');
	}
	$model = "edit";
	$position = "修改文章";
	$id = $_GET['id'];
	$sql = "SELECT * FROM articles WHERE id = ?";
	$stmt = $db->prepare($sql);
	$stmt->execute(array($id));
	$data = $stmt->fetch();	

	$sql = "SELECT * FROM images WHERE article_id = ?";
	$stmt = $db->prepare($sql);
	$stmt->execute(array($id));
	$images = $stmt->fetchAll();	
	if(!isset($data) || $data['user_id'] != $_SESSION['user']['id']){
		header('location: index.php');
	}
	//csrf
	$key = sha1(microtime());
	$_SESSION['csrf']['edit'] = $key; 
}else{
	$model = "create";
	$position = "新增文章";
	//csrf
	$key = sha1(microtime());
	$_SESSION['csrf']['create'] = $key;
}
?>
<section id="body">

<div class="container">
	<div class="row mt-5">
		<nav>
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
		    <li class="breadcrumb-item active"><?=@$position?></li>
		  </ol>
		</nav>
	</div>
	<div class="row">
	<?php
	if($model == "create"){
	?>
		<form id="form1" class="col-12" action="store.php?a=createArticle" method="post" enctype="multipart/form-data">	
			<div class="tab-content">
			  <div class="tab-pane show active" id="home">
			  	<?php
			  	if(isset($_SESSION['error'])){
			  	?>
			  	<div class="alert alert-danger" role="alert">
				  <?=pr($_SESSION['error']['error'])?>
				</div>
				<?php } ?>
			  	<div class="row">
				  	<div class="form-group col-12 col-lg-8">
				  		<label><h5><i class="fas fa-edit"></i> 標題</h5></label>
						<input class="form-control" type="text" name="title" placeholder="請輸入標題" value="<?=@pr($_SESSION['form']['title'])?>">
					</div>
					<div class="form-group col-6 col-lg-4">
						<label><h5><i class="fas fa-tags"></i> 地區分類</h5></label>
						<select name="region" class="form-control">
							<?php
							for ($i = 0 ; $i < count($locationNames) ; $i++) {
								echo '<option value="'.pr($i).'">'.pr($locationNames[$i]).'</option>';
							}
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label><h5><i class="far fa-image"></i> 封面相片</h5></label>
					<input type="file" name="cover" id="coverImg" class="form-control coverImg">

				</div>
				<div class="row" id="articleCover">
				</div>
				<hr>
				<div class="form-group">
					<label><h5><i class="fas fa-edit"></i> 內文</h5></label>
					<textarea name="content" class="col-12 form-control" rows="20"><?=@pr($_SESSION['form']['content'])?></textarea>
				</div>
				<hr>
				
				<div class="form-group">
					<label><h5><i class="far fa-images"></i> 上傳圖片</h5></label>
					<input class="form-control" type="file" id="imgInp" name="images[]" multiple="multiple">
				</div>
				<div id="imgPre" class="row">
				</div>
			  </div>
			</div>	
			
			<div class="col-12 text-center mt-5">
				<button type="submit" class="ml-2 btn btn-primary">送出</button>
			</div>
		</form>
	<?php }elseif($model == "edit"){ ?>
		<form id="form1" class="col-12" action="store.php?a=editArticle&id=<?=pr($id)?>" method="post" enctype="multipart/form-data">	
			<div class="tab-content">
			  <div class="tab-pane show active" id="home">
			  	<?php
			  	if(isset($_SESSION['error'])){
			  	?>
			  	<div class="alert alert-danger" role="alert">
				  <?=pr($_SESSION['error']['error'])?>
				</div>
				<?php } ?>
			  	<div class="row">
				  	<div class="form-group col-12 col-lg-8">
				  		<label><h5><i class="fas fa-edit"></i> 標題</h5></label>
						<input class="form-control" type="text" name="title" placeholder="請輸入標題" value="<?=$data['title']?>">
					</div>
					<div class="form-group col-6 col-lg-4">
						<label><h5><i class="fas fa-tags"></i> 地區分類</h5></label>
						<select name="region" class="form-control">
							<?php
							for ($i = 0 ; $i < count($locationNames) ; $i++) {
								$i == $data['region'] ? $selected = "selected" : $selected = "";
								echo '<option value="'.pr($i).'" '.pr($selected).'>'.pr($locationNames[$i]).'</option>';
							}
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label><h5><i class="far fa-image"></i> 封面相片</h5></label>
					<br>
					<div class="form-check mt-2">
					  <input class="form-check-input" type="radio" name="changeCover" id="unchangeCover" value="n" checked>
					  <label class="form-check-label" for="unchangeCover">
					    不更換圖片
					  </label>

					</div>
					<div class="form-check mb-2">
					  <input class="form-check-input" type="radio" name="changeCover" id="changeCover" value="y">
					  <label class="form-check-label" for="changeCover">
					    更換圖片
					  </label>
					</div>
					<input type="file" name="cover" id="coverImg" class="form-control coverImg">

				</div>
				<div class="row" id="articleCover">
					<?php
					if($data['cover'] != ""){
						echo '<div class="col-12"><img class="col-12 imgBox" name="coverImg" id="coverImg" src="'.COVER_DIR.pr($data['cover']).'"></div>';
					}
					?>
				</div>
				<hr>
				<div class="form-group">
					<label><h5><i class="fas fa-edit"></i> 內文</h5></label>
					<textarea name="content" class="col-12 form-control" rows="20"><?=pr($data['content'])?></textarea>
				</div>
				<hr>
				
				<div class="form-group">
					<label><h5><i class="far fa-images"></i> 上傳圖片</h5></label>
					<input class="form-control" type="file" id="imgInp" name="images[]" multiple="multiple">
				</div>
				<div class="row">
					<?php
					foreach ($images as $image) {
					?>
					<div class="col-12 col-md-4 mb-4">
						<div class="card">
						  <img class="card-img-top" src="<?=IMG_DIR.pr($id).'/'.pr($image['filename'])?>" alt="Card image cap">
						  <ul class="list-group list-group-flush">
						    <li class="list-group-item">
						    	<center><input class="form-check-input" name="deleteImg[]" type="checkbox" value="<?=pr($image['id'])?>">
								<label class="form-check-label" >
								   移除
								</label></center>
						    </li>

						  </ul>
						</div>
					</div>
					<?php } ?>
				</div>

				<div id="imgPre" class="row">
				</div>
			  </div>
			</div>	
			
			<div class="col-12 text-center mt-5">
				<button type="submit" class="ml-2 btn btn-primary">送出</button>
			</div>
		</form>
	<?php } ?>
	</div>
</div>
</section>
<?php
include 'footer.php';
?>