<?php
include 'include/config.php';
if(isset($_SESSION['user'])){
	header('location: index.php');
}
?>
<html>
<head>
	<title>Walk walk!</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link rel="stylesheet" href="asset/css/css.css"></link>
</head>
<body>
<section id="loginJumbotron" class="jumbotron jumbotron-fluid d-flex justify-content-center align-items-center">
	<div class="container">
		<h1 class="display-4 mb-3 text-center"><a href="index.php">Walk walk!</a></h1>
		<div id="formBox" class="col-lg-6 offset-lg-3 mb-3">
			<form action="auth.php?a=admin" method="POST">
				<div class="form-group mb-3 text-center">
					<label><h3>管理後臺</h3></label>
				</div>
				<div class="form-group">
					
					<label>Account</label>
					<input name="acc" type="text" class="form-control" id="acc" placeholder="Enter account">
					
				</div>
				<div class="form-group">
					<label>Password</label>
					<input name="passwd" type="password" class="form-control" id="passwd" placeholder="Enter password">
					<?php
					if(isset($_SESSION['error']['login'])){
						echo "<label class='mt-1 text-danger'>".pr($_SESSION['error']['login'])."</label>";
					}
					?>
				</div>
				<div class="form-group text-right">
					<button type="submit" class="btn btn-primary">Login</button>
				</div>
			</form>
		</div>

	</div>

</section>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="asset/js/normal.js"></script>
<script type="text/javascript" src="asset/js/article.js"></script>
</html>