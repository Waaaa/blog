<?php
include 'header.php';
if(!isset($_GET['id'])){
	header('location: index.php');
}
$id = $_GET['id'];

//抓出文章
$sql = "SELECT * FROM articles WHERE id = ?";
$stmt = $db->prepare($sql);
$res = $stmt->execute(array($id));
$data = $stmt->fetch();
if(!$data){
	header('location: index.php');
}


//抓出文章作者
$author_id = $data['user_id'];
$sql = "SELECT id, name, nickname, introduce, image FROM users WHERE id = ?";
$stmt = $db->prepare($sql);
$stmt->execute(array($author_id));
$author = $stmt->fetch();

//抓出文章圖片
$cover = $data['cover'];
$sql = "SELECT * FROM images WHERE article_id = ?";
$stmt = $db->prepare($sql);
$stmt->execute(array($id));
$images = $stmt->fetchAll();

$region = $locations[$data['region']];
$regionname = $locationNames[$data['region']];
?>
<section id="body">
	<?php
	if(count($images) > 0){
	?>
	<div id="carousel" class="carousel slide mb-5 col-12" data-ride="carousel">
	  <ol class="carousel-indicators">  
	    <?php
	    $i = 0;
	    foreach ($images as $image) {
	    	$i == 0 ? $active = 'active' : $active = '';
	    	echo '<li data-target="#carousel" data-slide-to="'.pr($i).'" class="'.pr($active).'"></li>';
	    	$i++;
	    }
	    ?>
	  </ol>
	  <div class="carousel-inner">
	  	<?php
	    for($i = 0 ; $i < count($images); $i++){
	    	$i == 0 ? $active = 'active' : $active = '';
	    	echo '<div class="carousel-item '.pr($active).'"><img class="d-block h-100" src="'.IMG_DIR.pr($id).'/'.pr($images[$i]["filename"]).'"></div>';
	    }
	    ?>
	  </div>
	  <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>
	<?php }else{ ?>
	<div id="articleJumbotron" class="jumbotron mb-5 d-flex justify-content-center align-items-center" style="background-image: url('<?=COVER_DIR.pr($cover)?>')">
	</div>
	<?php } ?>
	
	<div class="container mb-5">
		<div class="row">
			<nav>
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
			    <li class="breadcrumb-item"><a href="index.php?region=<?=pr($region)?>"><?=pr($regionname)?></a></li>
			    <li class="breadcrumb-item active"><?=pr($data['title'])?></li>
			  </ol>
			</nav>
		</div>
		<div class="row">
			<div class="col-lg-12 articleTitle">
				<label><h1><?=pr($data['title'])?></h1></label>
				<hr>
				<div class="text-right"><label>更新日期 : <?=pr($data['create_at'])?></label></div>				
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-lg-12">
				<div class="articleBody">
					<?=pr($data['content'])?>
				</div>
				<div id="userDetailBox" class="mt-5">
					<div class="col-lg-12 mb-2">
						<div class="row">
							<?php
							$author['introduce'] == "" ? $introduce = "<p class='text-secondary'>Oops! 看來作者還沒更新自我介紹 :( </p>" : $introduce = pr($author['introduce']);
							$author['nickname'] == "" ? $nickname = "" : $nickname = "( ".$author['nickname']." )";
							if($author['image'] != ""){

							?>
							<div id="userImageForm" class="col-4 pt-2">
								<img id="userImage" class="col-lg-12 mb-2 imgBox" src="<?=USER_IMG_DIR.pr($author['image'])?>">
							</div>
							<div class="col-8" id="userDetailForm">

								<div class="form-group">	
									<label class="pt-2 mb-0"><h4>關於作者</h4></label>	
									<hr>															
									<a href="personal.php?user=<?=pr($author['id'])?>" class="text-black"><h4 class="pt-2 mb-0"><?=pr($author['name'])?> <small class="text-secondary"><?=pr($nickname)?></small></h4></a>
									
									<br>
									<div><?=$introduce?></div>
								</div>
							</div>
							<?php
							}else{
							?>
							<div class="col-12" id="userDetailForm">
								<div class="form-group">
									<label class="pt-2 mb-0"><h4>關於作者</h4></label>
									<hr>
									<div>
										<a href="personal.php?user=<?=pr($author['id'])?>" class="text-black"><h4 class="pt-2 mb-0"><?=pr($author['name'])?> <small class="text-secondary"><?=pr($nickname)?></small></h4></a>
										<br>
										<?=$introduce?>		
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<?php
include 'footer.php';
?>