<?php
define('DB_HOST', 'YOUR_DB_HOST');
define('DB_NAME', 'YOUR_DB_NAME');
define('DB_USER', 'YOUR_DB_USERNAME');
define('DB_PASSWD', "YOUR_DB_PASSWORD");
define('IMG_DIR', "asset/images/articles/");
define('COVER_DIR', "asset/images/covers/");
define('USER_IMG_DIR', "asset/images/userimages/");
$locations = ['north', 'center', 'south', 'east', 'island'];
$locationNames = ['臺灣北部', '臺灣中部', '臺灣南部', '臺灣東部', '離島地區'];
$breaks = array("<br />","<br>","<br/>"); 

session_start();

include('connect.php');
include('lib.php');

checkUser($db);