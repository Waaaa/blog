<?php
function pr($str){
	return nl2br(htmlentities($str));
}

function checkUser($db){
	if(isset($_SESSION['user']) && $_SESSION['user']['id'] != 0){
		$sql = "SELECT id, name, nickname, mail, introduce, sex, image, authority FROM users WHERE id = ?";
		$stmt = $db->prepare($sql);
		$stmt->execute(array($_SESSION['user']['id']));
		$data = $stmt->fetch();
		if(!empty($data) && !$data['authority']){
			$_SESSION['user'] = $data;
		}else{
			unset($_SESSION['user']);
			header('location: index.php');
			exit;
		}
		
	}
}