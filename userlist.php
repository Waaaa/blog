<?php
include 'header.php';

$id = $_SESSION['user']['id'];
if($id != 0){
	header('location: index.php');
}else{
	$data = $db->query("SELECT * FROM users")->fetchAll();
}




?>
<section id="body">
	<div class="container mb-5">
		<div class="row mt-5">
			<nav>
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
			    <li class="breadcrumb-item">使用者管理文章</li>
			    <li class="breadcrumb-item active">使用者列表</li>
			  </ol>
			</nav>
		</div>
		<div class="row" id="listBox">
			<?php
			if(count($data) == 0){
				echo '<div class="emptyCategory col-lg-12 text-center">你還沒有新增任何的文章喔!</div>';
			}
			?>
			<table class="table">
				<?php
				for($i = 0 ; $i < count($data); $i++){
					echo "<tr >";
					$noborder = '';
					if($i == 0){
						$noborder = 'style="border-top: 0;"';
					}
					$index = $i + 1;
					$onclick = 'return confirm("確定要刪除使用者嗎? 系統將無法復原您的刪除")';
					echo '<th '.$noborder.' scope="row">'.pr($index).'</th>';
					echo "<td ".$noborder.">".pr($data[$i]['mail'])."</td>";
					echo "<td ".$noborder.">".pr($data[$i]['name'])."</td>";
					echo "<td ".$noborder.">".pr($data[$i]['nickname'])."</td>";
					if($data[$i]['authority']){
						echo "<td ".$noborder."><a href='store.php?a=editUser&id=".pr($data[$i]['id'])."'>復原登入權限</a></td>";						
					}else{
						echo "<td ".$noborder."><a href='store.php?a=editUser&id=".pr($data[$i]['id'])."'>停權</a></td>";							
					}
					
					echo "<td ".$noborder."><a href='store.php?a=deleteUser&id=".pr($data[$i]['id'])."' onclick='".$onclick."'>刪除</a></td>";
					echo "</tr >";
				}
				?>
			</table>
		</div>

	

	</div>

</section>
<?php
include 'footer.php';
?>