function coverReadURL(input) {
    //alert(input.files.length);
    if (input.files && input.files[0]) {
      $('#articleCover').html('');
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#articleCover').append('<div class="col-12"><img class="col-12 imgBox" name="coverImg" id="coverImg" src="'+e.target.result+'"></div>');
        }
        reader.readAsDataURL(input.files[0]);
      
    }
}

function albumReadURL(input) {
    //alert(input.files.length);
    if (input.files && input.files.length > 0) {
      $('#imgPre').html('');
      for(i = 0 ; i < input.files.length ; i++){

        var reader = new FileReader();

        reader.onload = function(e) {
          $('#imgPre').append('<div class="col-12 col-md-4 mb-4"><img class="col-12 imgBox" name="coverImg" id="coverImg" src="'+e.target.result+'"></div>');
        }
        reader.readAsDataURL(input.files[i]);

      }
      
      
    }
}

$("#coverImg").change(function() {
  coverReadURL(this);
});
$("#imgInp").change(function() {
  albumReadURL(this);
});