/* personal */

$("#editDateBtn").click(function(event) {
	$("#userDetailForm input").removeAttr("readonly");
	$("#userDetailForm textarea").removeAttr("readonly");
	$(this).attr("disabled","disabled");
	$("#button-group").append('<button type="submit" class="btn btn-primary">送出</button>')
});
$("#userImageInput").on("change",function(){
	ReadImg(this, '#userImage');
});

function ReadImg(input, img_id){
	if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) {
	      $(img_id).attr('src', e.target.result);
	      $(img_id).css('width', '100%');
	    }

	    reader.readAsDataURL(input.files[0]);
	 }
}

/* -- */

